#!/usr/bin/env python3

import sys

with open('input/task1.txt', 'r') as data:
    changes = [int(x) for x in data.read().strip().split("\n")]


freq = 0
seen = set()
while True:
    for n in changes:
        freq = freq + n
        if freq in seen:
            print(freq)
            sys.exit()
        seen.add(freq)