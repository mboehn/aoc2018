#!/usr/bin/env python3

import sys


def count_diff(str1, str2):
    z = zip(str1, str2)

    same = ""

    count = 0

    for a, b in z:
        if a != b:
            count += 1
        else:
            same = same + a
    

    return (count, same)


if __name__ == '__main__':
    with open('input/task2.txt', 'r') as data:
        boxes = [str(x) for x in data.read().strip().split("\n")]

    for box in boxes:
        for innerbox in boxes:
            diff, same = count_diff(box, innerbox)
            if diff == 1:
                print(box, innerbox)
                print(same)
