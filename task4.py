#!/usr/bin/env python3

from pprint import pprint
import re
from datetime import datetime
import collections

import utils


if __name__ == "__main__":
    data  = utils.init()
    
    newdata = list()
    for line in data['input'].split('\n'):
        s = re.search(r'\[(.*)\] (.*)', line)
        newdata.append((
            datetime.strptime(s.group(1), '%Y-%m-%d %H:%M'),
            s.group(2),
        ))

    newdata.sort(key = lambda x: x[0])


    part2_minutes = list()
    guards_sum = dict()
    guards_minutes = dict()
    for line in newdata:
        guard_search = re.search(r'^Guard #(\d+) begins shift$', line[1])
        if guard_search:
            guard = guard_search.group(1)
        
        if 'falls asleep' in line[1]:
            start = line[0].minute
            #print(guard + ' asleep ' + str(start))
        if 'wakes up' in line[1]:
            end = line[0].minute
            duration = end - start
            #print(guard + ' wakes up ' + str(end) + ' slept for ' + str(duration))
            guards_sum[guard] = guards_sum.get(guard, 0) + duration

            if guard not in guards_minutes:
                guards_minutes[guard] = list()
            guards_minutes[guard] = guards_minutes[guard] + list(range(start, end))

            for i in range(start, end):
                part2_minutes.append((guard, i))



    sorted_guards = sorted(guards_sum.items(), key=lambda kv: kv[1])
    print("Guard #{} slept the most: {} minutes".format(sorted_guards[-1][0], sorted_guards[-1][1]))

    minutes_counted = collections.Counter(guards_minutes[sorted_guards[-1][0]])
    result = max(minutes_counted.items(), key=lambda x: x[1])

    print("Guard #{} slept most during minute {} ({} times)".format(sorted_guards[-1][0], result[0], result[1]))
    print("Part 1: {}".format(int(sorted_guards[-1][0]) * int(result[0])))

    part2_counted = collections.Counter(part2_minutes)
    result2 = max(part2_counted.items(), key=lambda x: x[1])
    #print(result2)
    print("Guard #{} slept {} times during minute {}".format(
        result2[0][0],
        result2[1],
        result2[0][1],
    ))
    print("Part 2: {}".format(int(result2[0][0]) * int(result2[0][1])))