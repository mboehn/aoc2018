# -*- coding: utf-8 -*-

import os
import re
import __main__ as main

import requests

AOC_YEAR = '2018'
INPUT_DIR = 'input/'
INPUT_PREFIX = 'task'
INPUT_SUFFIX = '.txt'
USER_AGENT = 'utils.py by mboehn'
SESSION_ENV = 'AOC_SESSION'
AOC_URL = 'https://adventofcode.com/{year}/day/{task}/input'


def get_input(task):
    inputfile = os.path.join(INPUT_DIR, INPUT_PREFIX + task + INPUT_SUFFIX)

    if os.path.isfile(inputfile):
        with open(inputfile, 'r') as f:
            return f.read().rstrip('\r\n')

    session = os.environ.get(SESSION_ENV)
    if not session:
        raise ValueError('Missing env variable {}'.format(SESSION_ENV))
    
    response = requests.get(
        url=AOC_URL.format(year=AOC_YEAR, task=task),
        cookies={'session': session},
        headers={'User-Agent': USER_AGENT}
    )
    if not response.ok:
        raise Exception('Web request failed: {}'.format(response.status_code))
    
    input_data = response.text
    with open(inputfile, 'w') as f:
        f.write(input_data)
    
    return input_data.rstrip('\r\n')


def init():
    data = {}
    filename = main.__file__.split('/')[-1]
    search = re.search(r'^task(\d+)(\w*)\.py$', filename)
    if not search:
        raise ValueError('Filename not correct: {}'.format(main.__file__))
    
    data['task'] = search.group(1)
    data['input'] = get_input(data['task'])

    return data