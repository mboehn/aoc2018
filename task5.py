#!/usr/bin/env python3

import operator
import string as fuck_naming_string
import sys
from pprint import pprint
import re
from datetime import datetime
import collections

import utils


def parse(string, char_to_remove=None):

    if char_to_remove:
        string = string.replace(c, "")
        string = string.replace(c.swapcase(), "")

    string = list(string)

    #loops = 0
    while(True):
        #loops += 1
        #print("Loop number {}".format(loops))
        for n, l in enumerate(string):
            try:
                if string[n+1] == l.swapcase():
                    #print("current is {}, next is {}".format(l, l.swapcase()))
                    #print(string[n], string[n+1])
                    del string[n+1]
                    del string[n]
                    
                    break
            except IndexError:
                #print("might be done...")
                #print(string)
                return len(string)


if __name__ == "__main__":
    data  = utils.init()
    
    #part1 = parse(data['input'])
    #print("Part1: {}".format(part1))


    part2 = {}
    for c in fuck_naming_string.ascii_lowercase:
        part2[c] = parse(data['input'], c)

    part2_result = min(part2.items(), key=operator.itemgetter(1))
    print("Part 2: poly {}/{} count {}".format(
        part2_result[0],
        part2_result[0].swapcase(),
        part2_result[1]
    ))