#!/usr/bin/env python3

with open('input/task1.txt', 'r') as data:
    changes = [int(x) for x in data.read().strip().split("\n")]

print(sum(changes))