#!/usr/bin/env python3

import sys
from collections import Counter

with open('input/task2.txt', 'r') as data:
    boxes = [str(x) for x in data.read().strip().split("\n")]


count_two = 0
count_three = 0

for box in boxes:
    found_two = False
    found_three = False

    lettercount = Counter(letter for letter in box)
    for key, value in lettercount.items():
        if value == 2:
            found_two = True
        if value == 3:
            found_three = True
    
    if found_two:
        count_two += 1
    if found_three:
        count_three += 1
    
    #input()


print(count_two)
print(count_three)
print(count_two * count_three)