#!/usr/bin/env python3
# *-* coding: utf-8 *-*

import collections
import re

import utils

#INPUT_FILE = 'input/task3.txt'
coordlist = []


def parseclaim(line):
    claim = {}
    search = re.search(r'^#(\d+) @ (\d+),(\d+): (\d+)x(\d+)$', line)
    claim['id'] = int(search.group(1))
    claim['leftmargin'] = int(search.group(2))
    claim['topmargin'] = int(search.group(3))
    claim['width'] = int(search.group(4))
    claim['height'] = int(search.group(5))
    return claim


def expandclaim(claim):
    left = claim['leftmargin']
    top = claim['topmargin']
    width = claim['width']
    height = claim['height']

    right = left + width
    bottom = top + height

    # Y i sky, X til.. høyre?
    claim['coords'] = []
    for x in range(left, right):
        for y in range(top, bottom):
            coords = "{},{}".format(x, y)
            coordlist.append(coords)
            claim['coords'].append(coords)

    return claim


def main():
    task_data = utils.init()

    claims = []
    for line in task_data['input'].split('\n'):
        claim = parseclaim(line)
        claim = expandclaim(claim)
        claims.append(claim)

    total_count = 0
    coordcount = collections.Counter(coordlist)
    for _, count in coordcount.items():
        if count > 1:
            total_count += 1
    print("part 1: {}".format(total_count))

    for claim in claims:
        for coord in claim['coords']:
            if coordcount[coord] > 1:
                claim['breaked'] = True
                break
            else:
                claim['breaked'] = False
        
        if not claim['breaked']:
            print("part 2: {}".format(claim['id']))


if __name__ == '__main__':
    main()